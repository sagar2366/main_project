const axios = require('axios');
const argv = require('yargs').argv
var internal_projects = [];
var private_projects = [];
var public_projects = [];
var username = argv.name;
var private_token = argv.token;
axios.get(`https://gitlab.com/api/v4/users/${username}/projects`, {
  headers : {
    'PRIVATE-TOKEN': private_token
  }
})
  .then(response => {
    console.log("..................................................")
    response.data.forEach(element => {
      if(element.visibility === "internal"){
        internal_projects.push(element.name)
      }
      else if(element.visibility === "private"){
        private_projects.push(element.name);
      }
      else{
        public_projects.push(element.name);
      }
    });
    console.log("...................Internal Projects...............................");
    print_projects(internal_projects);
    console.log("...................Private Projects...............................");
    print_projects(private_projects);
    console.log("...................Public Projects...............................")
    print_projects(public_projects);
    console.log(".................................................................")
  })
  .catch(error => {
    console.log(error);
  });

  function print_projects(project_array) {
    project_array.forEach((i,index) => {
      console.log(index+1,i);
    })
  }